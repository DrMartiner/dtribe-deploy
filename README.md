# Links

## Admin
- [Kibana logs](http://54.242.168.63/admin/logs/) - admin OQAQRCc8n2
- [Django admin](http://54.242.168.63/admin/django/) - admin@admin.com 80gnApLqDF


## API

### Auto-docs
[Swagger](http://54.242.168.63/api/v1/docs/swagger/)

### Categories
[Categories](http://54.242.168.63/api/v1/common/category/)

### News
- [News](http://54.242.168.63/api/v1/news/)
- [Source](http://54.242.168.63/api/v1/news/source/)
- [vimeo-video](http://54.242.168.63/api/v1/news/vimeo-video/)
- [youtube-video](http://54.242.168.63/api/v1/news/youtube-video/)
- [vimeo-audio](http://54.242.168.63/api/v1/news/vimeo-audio/)
- [youtube-audio](http://54.242.168.63/api/v1/news/youtube-audio/)
- [apple-cast](http://54.242.168.63/api/v1/news/apple-cast/)
- [sound-cloud](http://54.242.168.63/api/v1/news/sound-cloud/)

### Tribe
- [tribe](http://54.242.168.63/api/v1/tribe/)

### Subscribe
- [subscribe](http://54.242.168.63/api/v1/subscribe/)

### User
- [user](http://54.242.168.63/api/v1/user/)
