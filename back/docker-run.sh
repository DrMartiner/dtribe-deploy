#!/bin/sh

pipenv install

pipenv run ./manage.py migrate
pipenv run ./manage.py loaddata project/fixtures/$ROLE/*
pipenv run ./manage.py collectstatic --noinput

rm -rf /tmp/celery*
export C_FORCE_ROOT='true'  # for run Celery
pipenv run celery worker -A project -E -Q email,media --beat &
pipenv run celery flower -A project --address=back --port=5555 --url_prefix=/admin/celery/ &

pipenv run gunicorn project.wsgi:application -w 2 --log-level=info --bind=back:8000 --reload
