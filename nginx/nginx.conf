user root;
worker_processes auto;

pid /run/nginx.pid;

events {
	worker_connections 1024;
}

http {
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	etag off;
	gzip_disable "msie6";

	charset utf-8;
    client_max_body_size 30m;
    client_body_buffer_size 128k;

    resolver 8.8.8.8 8.8.4.4 valid=300s;
    resolver_timeout 5s;

    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
    ssl_ecdh_curve secp384r1;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off;

    ssl_certificate /etc/ssl/cert/bundle.crt;
    ssl_certificate_key /etc/ssl/cert/private.pem;

    server {
        listen 80;
        server_name stage.dtribe.one;
        return 301 https://$host$request_uri;
    }

	server {
        listen 443 ssl;
        server_name stage.dtribe.one;

        set $ROOT_DIR /var/www;

        location /admin/logs/ {
            proxy_pass http://kibana:5601/;

            auth_basic "Restricted";
            auth_basic_user_file /etc/nginx/htpasswd/admin;
        }

        location /admin/celery/ {
            proxy_pass http://back:5555/;

            auth_basic "Restricted";
            auth_basic_user_file /etc/nginx/htpasswd/admin;
        }

        location ~ ^/(static|media)/*  {
            root $ROOT_DIR;
            include confs/static_params;
        }

        location / {
            proxy_pass  http://back:8000;
            include confs/proxy_params;
        }
    }
}
